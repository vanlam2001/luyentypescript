function c(name: string) {
  console.log(`Hello, ${name}!`);
}

c("TypeScript");

const renderText =  (name: string) =>{
    console.log(`Hello, ${name}!`);
}

renderText("Hello world");


const renderNumber = (number1: number, number2: number) => {
      console.log(`${number1} ${number2}`)
} 

renderNumber(1,2);

const renderToTal = (toan: number, van: number) =>{
      return toan + van 
}
console.log(renderToTal(1,2));

const age = 35;
const nextAge = age + 1;
console.log(nextAge);

const getUpperCaseName = (caseName: string) => {
     return caseName.toUpperCase();     
}

console.log(getUpperCaseName("Nguyen Van A"))

const isNumber = (number: number) => {
    return typeof number === 'number' && !Number.isNaN(number);
}

console.log(isNumber(999));

const numbers: number[] = [1, 2, 3];
console.log(numbers)

const strings: string[] = ['a', 'b', 'c'];
console.log(strings)

const cars : string[] = ['Honda', 'Mazada', 'Mercedes'];
const joinWithCharacter = (array: string[], charactor: string) => {
     return array.join(charactor)
}
console.log(joinWithCharacter(cars, '-'));

const animals: string[] = ['Monkey', 'Tiger', 'Elephant'];

const getLastElement = (array: string[]) => {
    return array[2];
}

const ketQua = getLastElement(animals)
console.log(ketQua);


const array1: string[] = ['Monkey', 'Tiger', 'Elephant'];


const getLastElement2 = (arr: string[]) => {
     return arr[arr.length - 1];
}

const ketqua = getLastElement2(array1)
console.log(ketqua);



interface Student  {
    name: string;
    age: number;
    email: string;
}

const alice: Student = {
    name: "alice nguyen",
    age: 12,
    email: "a@gmail.com"
}
console.log(alice)


class Animail{
    name: string;
    leg: number;
    speed: number;

    constructor(name: string, leg: number, speed: number){
       this.name = name;
       this.leg = leg;
       this.speed = speed;
    }
}

const parrot = new Animail('Parrot', 2, 20);
console.log(parrot)



class HocSinh{
    firstName: string;
    lastName: string;

    constructor( firstName: string, lastName: string){
       this.firstName = firstName;
       this.lastName = lastName;
    }

    getFullName(): string{
        return `${this.firstName} ${this.lastName}`;
    }
}


const student = new HocSinh('Long', 'Bui');
console.log(student.getFullName());


const getNextYear = () :number => {
    return new Date().getFullYear() + 1;
}

console.log(getNextYear())

const run = (a: number) => {
   if(a % 15 === 0){
      return 3
   } else if(a % 5 === 0){
      return 2
   } else if(a % 3 === 0){
      return 1
   }
}

console.log(run(3))
console.log(run(5))
console.log(run(15))


const run1 = (fruits: string) => {
    let result: string;

    switch(fruits){
      case "Banana": 
            result = "This is a Banana";
            break;
      case "Apple":
            result = "This is a Apple";
            break;
      default: 
            result = "No fruits";
    }
    return result;
}

console.log(run1("Apple"));

const getCanVoteMessage = (age: number) => {
      if(age >= 18){
        return "Bạn có thể bỏ phiếu"
      } else{
        return "Bạn không thể bỏ phiếu"
      }
}

console.log(getCanVoteMessage(18))
console.log(getCanVoteMessage(15))

const getRandNumbers = (min: number, max: number, length: number): number[] => {
      const arr: number[] = [];
      for(let i = 0; i < length; i++){
         let ranNums = Math.round(Math.random() * (max - min) + min);
         arr.push(ranNums);
      }

      return arr;
}

let tong = getRandNumbers(1,10,20);
console.log(tong);

const arrNumber: number[] = [4,5,3,5];
const getTotal = (arr: number[]) => {
     let number = 0;
     for(let i = 0; i < arr.length; i++){
          number += arr[i];
     }
     return number;
}
console.log(getTotal(arrNumber));

const orders: {name: string, price: number} [] = [
      {
        name: 'Khoá học HTML - CSS Pro',
        price: 3000000
      },
      {
        name: 'Khoá học Javascript Pro',
        price: 2500000
      },
      {
        name: 'Khoá học React Pro',
        price: 3200000
      }
];

const tinhTotal = (arr: {name:string, price: number}[]) => {
      let number = 0;
      for(let i = 0; i < arr.length; i++) {
           number += arr[i].price;
      }
      return number;
}
console.log(tinhTotal(orders));









